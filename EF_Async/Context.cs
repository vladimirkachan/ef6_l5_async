﻿using System;
using System.Data.Entity;
using System.Linq;

namespace EF_Async
{
    public class Context : DbContext
    {
        static Context()
        {
            Database.SetInitializer(new DropCreateDatabaseAlways<Context>());
        }
        public Context()
            : base("name=Context")
        {
        }
        public virtual DbSet<Phone> Phones { get; set; }
    }
}