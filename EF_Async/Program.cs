﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF_Async
{
    class Program
    {
        static async Task Main(string[] args)
        {
            using Context db = new();
            db.Phones.AddRange(new[]
            {
                new Phone{Name="LG Nexus 5", Price=4000},
                new Phone{Name ="Samsung Galaxy 6", Price = 3000},
                new Phone{Name="Nokia 930", Price=1000}
            });
            await db.SaveChangesAsync();
            Console.WriteLine($"Count = {await db.Phones.CountAsync()}");
            await db.Phones.ForEachAsync(p => Console.WriteLine(p));

        }
    }
}
